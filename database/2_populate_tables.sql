INSERT INTO sport(id, icon, name) 
	values 
	(1, '/000', 'Football'),
	(2, '/000', 'Basketball'),
	(3, '/000', 'Boxing');
	
INSERT INTO tournament(id, name, sport_id, scope, logo, organizer)
	values
	(1,'Futbol Liga 2', 1, 'Regional','/000', 'Raul Penilla'),
	(2,'Futbol Liga Estrellas', 1, 'Internacional', '/000', 'Emmanuel Young'),
	(3,'Solo Amateur', 2, 'Regional', '/000', 'Ricardo Perez'),
	(4,'Sangre', 3, 'Internacional', '/000', 'Brandon Jimenez');