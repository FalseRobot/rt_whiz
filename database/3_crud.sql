CREATE PROCEDURE SP_CREATE_TOURNAMENT
	(_id integer, 
	 _name varchar(255), 
	 _sport_id integer, 
	 _scope varchar(255), 
	 _logo bytea, 
	 _organizer varchar(255))
LANGUAGE SQL
AS $BODY$
    INSERT INTO tournament(id, name, sport_id, scope, logo, organizer)
	values
	(_id, _name, _sport_id, _scope, _logo, _organizer);
$BODY$;

CREATE PROCEDURE SP_DELETE_TOURNAMENT
	(_id integer)
LANGUAGE SQL
AS $BODY$
    DELETE FROM tournament
	WHERE id = _id;
$BODY$;

CREATE PROCEDURE SP_UPDATE_TOURNAMENT
	(_id integer, 
	 _name varchar(255), 
	 _sport_id integer, 
	 _scope varchar(255), 
	 _logo bytea, 
	 _organizer varchar(255))
LANGUAGE SQL
AS $BODY$
	UPDATE tournament as t
    SET 
	name = _name,
	sport_id = _sport_id,
	scope = _scope,
	logo = _logo,
	organizer = _organizer
	WHERE t.id = _id;
$BODY$;

CREATE FUNCTION public.GET_TOURNAMENT(
    _id integer)
    RETURNS TABLE(resultset jsonb) 
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    return QUERY
    select jsonb_build_object(
        'id', t.id,
        'name', t.name,
        'scope', t.scope,
        'logo', t.logo,
        'organizer', t.organizer,
        'sport_id', t.sport_id,
		'sport_icon', s.icon,
		'sport_name',s.name)
    FROM tournament AS t
	INNER JOIN sport as s
	ON t.sport_id = s.ID
	WHERE t.id = _id; 
END;
$BODY$;
		