CREATE TABLE sport(
    id integer PRIMARY KEY not null,
    icon bytea not null,
    name varchar(64) not null
);

CREATE TABLE tournament(
    id integer PRIMARY KEY,
    name varchar(255) not null,
    sport_id integer references sport(id),
    scope varchar(255) CHECK(scope in ('Regional', 'Internacional')),
    logo bytea not null,
    organizer varchar(255) not null
);
