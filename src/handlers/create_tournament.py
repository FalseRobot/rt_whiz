import json
import psycopg2
import os

connection = psycopg2.connect(host=os.environ['DB_HOSTNAME'], database=os.environ['DB_DATABASE'],user=os.environ['DB_USERNAME'], password=os.environ['DB_PASSWORD'])

def create_tournament(event, context):
    body = event['body']
    try:
        print('event ',event)
        with connection.cursor() as cursor:
            cursor.execute("""CALL public.SP_CREATE_TOURNAMENT(%s::integer, %s::varchar, %s::integer, %s::varchar, %s::bytea, %s::varchar);""",(
                body.get('id'), 
                body.get('name'), 
                body.get('sport_id'), 
                body.get('scope'), 
                body.get('logo'),
                body.get('organizer'),
                ))
            connection.commit()
        return {
              'msg': 'tournament Created'
        }
    except (Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        raise RuntimeError('Error DB')