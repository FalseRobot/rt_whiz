import json
import psycopg2
import os

connection = psycopg2.connect(host=os.environ['DB_HOSTNAME'], database=os.environ['DB_DATABASE'],user=os.environ['DB_USERNAME'], password=os.environ['DB_PASSWORD'])

def delete_tournament(event, context):
    try:
        print('event ',event)
        with connection.cursor() as cursor:
            cursor.execute("""CALL public.SP_DELETE_TOURNAMENT(%s::integer);""",(event['id']))
            connection.commit()
        return {
              'msg': 'tournament Deleted'
        }
    except (Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        raise RuntimeError('Error DB')