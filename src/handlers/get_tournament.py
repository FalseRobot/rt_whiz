import json
import psycopg2
import os

connection = psycopg2.connect(host=os.environ['DB_HOSTNAME'], database=os.environ['DB_DATABASE'],user=os.environ['DB_USERNAME'], password=os.environ['DB_PASSWORD'])

def get_tournament(event, context):
    try:
        print('event ',event)
        with connection.cursor() as cursor:
            cursor.callproc("public.GET_TOURNAMENT", (event['id']))
            result = cursor.fetchone()[0]
        return result
    except (Exception, psycopg2.DatabaseError) as error:
        raise RuntimeError('Error DB')